﻿// <copyright file="DelayScrubber.cs" company="Eon du Plessis">minDelay
//   Copyright &copy; Eon du Plessis 2014 - all rights reserved
// </copyright>

namespace CheckInternetOnline.Helpers
{
    using System;

    /// <summary>
    /// Responsible to check if delay options (which in my application will always be the first option if the type = "o") is between 125 and 5000
    /// </summary>
    public struct DelayScrubber
    {
        /// <summary>
        /// Minimum Delay
        /// </summary>
        private const int MinDelay = 125;

        /// <summary>
        /// Maximum Delay
        /// </summary>
        private const int MaxDelay = 5000;

        /// <summary>
        /// Checks if delay is between 125 and 5000 milliseconds.
        /// </summary>
        /// <param name="options">The original command options</param>
        /// <returns>A scrubbed delay for the thread to sleep</returns>
        internal static int SetDelay(string[] options)
        {
            int delay;
            string option = ReturnFirstOption(options);
            
            try
            {
                return (!int.TryParse(option, out delay)) ? MinDelay : 
                    (delay < MinDelay) ? MinDelay : 
                    (delay > MaxDelay) ? MaxDelay : 
                    delay;
            }
            catch (OverflowException)
            {
                // A good thought: Try to make your application still work even after an exception is thrown. Utilize Defaults.
                return MinDelay;
            }
        }

        /// <summary>
        /// Checks if more than one option was defined, and returns the first option
        /// </summary>
        /// <param name="options">The original command options</param>
        /// <returns>First option as string, or empty string</returns>
        private static string ReturnFirstOption(string[] options)
        {
            return (options.Length > 1) ? options[1] : string.Empty;
        }
    }
}
