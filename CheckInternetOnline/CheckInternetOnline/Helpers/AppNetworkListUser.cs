﻿// <copyright file="AppNetworkListUser.cs" company="Eon du Plessis">
//   Copyright &copy; Eon du Plessis 2014 - all rights reserved
// </copyright>

namespace CheckInternetOnline.Helpers
{
    using System;
    using NETWORKLIST;

    /// <summary>
    /// Helper class to check if internet is online by utilizing the NetworkListManager DLL COM Object.
    /// </summary>
    public class AppNetworkListUser
    {
        /// <summary>
        /// Required interface to check if network is currently available.
        /// </summary>
        private readonly INetworkListManager mnlm;

        /// <summary>
        /// Initializes a new instance of the <see cref="AppNetworkListUser"/> class.
        /// </summary>
        public AppNetworkListUser()
        {
            this.mnlm = new NetworkListManager();
        }

        /// <summary>
        /// Gets the networkListManager variable.
        /// </summary>
        public INetworkListManager NLM
        {
            get
            {
                return this.mnlm;
            }
        }
    }
}
