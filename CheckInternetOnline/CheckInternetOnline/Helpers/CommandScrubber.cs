﻿// <copyright file="CommandScrubber.cs" company="Eon du Plessis">
//   Copyright &copy; Eon du Plessis 2014 - all rights reserved
// </copyright>

namespace CheckInternetOnline.Helpers
{
    using System;
    using System.Text;
    using System.Text.RegularExpressions;

    /// <summary>
    /// Takes a Dirty Command and formats it to be easily used in most situations
    /// </summary>
    public static class CommandScrubber
    {
        /// <summary>
        /// Cleans the command and returns an array containing each argument of the command
        /// </summary>
        /// <param name="dirtyCommand">The raw command to be scrubbed</param>
        /// <returns>An array of arguments which the command comprises of.</returns>
        public static string[] ScrubCommand(string dirtyCommand)
        {
                return Regex.Replace(dirtyCommand, @"\s+", " ").Split(new char[1] { ' ' });
        }
    }
}
