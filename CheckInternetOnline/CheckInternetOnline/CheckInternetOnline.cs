﻿//-------------------------------------------------------
// <copyright file="CheckInternetOnline.cs" company="EonduPlessis">
//   Copyright Eon du Plessis 2014 - All Rights Reserved.
// </copyright>
//-------------------------------------------------------

namespace CheckInternetOnline
{
    using System;
    using System.Text;
    using System.Threading;
    using Helpers;
    using Modules;

    /// <summary>
    /// Application to check if internet is currently online or offline.
    /// </summary>
    internal class CheckInternetOnline
    {
        /// <summary>
        /// Thread for the Internet Checker Module
        /// </summary>
        private static Thread checkInternetThread = new Thread((n) => Checker.CheckInternet(CommandScrubber.ScrubCommand(n.ToString())));

        /// <summary>
        /// Entry point for application.
        /// </summary>
        public static void Main()
        {
            DoIntro();
        }

        /// <summary>
        /// This Screen Welcomes the user and Displays the commands available to him to use.
        /// </summary>
        private static void DoIntro()
        {
            var introString = @"Welcome to CheckInternetOnline, a free tool to check if your internet is online.
To Start:
Type 'o <frequency in milliseconds, starting at 125 ms>'
Type 's' to Stop the internet checker
Type 'q' to Quit
Please input a command or type ""help""";
            Console.WriteLine(introString);
            AwaitCommand();
        }

        /// <summary>
        /// This method handles the user's input to the application.
        /// </summary>
        private static void AwaitCommand()
        {
            while (true)
            {
                var input = Console.ReadLine();

                if (string.Equals("q", input, StringComparison.OrdinalIgnoreCase))
                {
                    Console.WriteLine("Goodbye!");

                    // Better to use Environment.Exit(0) than Application.Exit() in Console applications.
                    // Runtime Exception will occur if you do not have SecurityPermissions.UnmanagedCode.
                    Environment.Exit(0);
                }

                if (input.StartsWith("o", StringComparison.OrdinalIgnoreCase))
                {
                    if (!checkInternetThread.IsAlive)
                    {
                        // This line of code seems familiar, doesn't it? It is recreated because once a thread is killed, and not in a threadpool, it cannot be reused. 
                        // The shortest and easiest fix is to... Blimey - Recreate it.
                        checkInternetThread = new Thread((n) => Checker.CheckInternet(CommandScrubber.ScrubCommand(n.ToString())));
                        Checker.IsRunning = true;
                        checkInternetThread.Start(input);
                    }
                }

                if (input.StartsWith("s", StringComparison.OrdinalIgnoreCase))
                {
                    if (checkInternetThread.IsAlive)
                    {
                        Console.WriteLine("Ending Check Internet");
                        Checker.IsRunning = false;
                        checkInternetThread.Join();
                    }
                }
            }
        }
    }
}
