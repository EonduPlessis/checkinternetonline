﻿// --------------------------------------------------------------
// <copyright file="AssemblyInfo.cs" company="Eon du Plessis">
//   Copyright &copy; Eon du Plessis 2014 - All Rights Reserved
// </copyright>
// --------------------------------------------------------------
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("CheckInternetOnline")]
[assembly: AssemblyDescription("Lightweight app to check if internet is online or not with additional built-in functionality")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Eon du Plessis")]
[assembly: AssemblyProduct("CheckInternetOnline")]
[assembly: AssemblyCopyright("Copyright © Eon du Plessis 2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("60f0b70f-d6c1-48a8-bdad-76457432ba85")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("0.0.1.*")]
[assembly: AssemblyFileVersion("1.0.0")]
