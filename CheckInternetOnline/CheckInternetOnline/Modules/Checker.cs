﻿// <copyright file="Checker.cs" company="Eon du Plessis">
//   Copyright &copy; Eon du Plessis 2014 - all rights reserved
// </copyright>

namespace CheckInternetOnline.Modules
{
    using System;
    using System.Media;
    using System.Reflection;
    using System.Resources;
    using System.Threading;
    using System.Web;
    using Helpers;

    /// <summary>
    /// Core class to check if internet is up and available
    /// </summary>
    public static class Checker
    {
        /// <summary>
        /// Gets or sets a value indicating whether the flag IsRunning which checks if Infinite Loop for CheckInternet should run or not
        /// </summary>
        public static bool IsRunning { get; set; }

        /// <summary>
        /// Utilizing options, this will check the current state of the internet.
        /// </summary>
        /// <param name="options">Controls behavior of internet checker</param>
        public static void CheckInternet(string[] options)
        {
            bool online = false;
            var resourceManager = new ResourceManager("CheckInternetOnline.CheckInternetOnlineResources", Assembly.GetExecutingAssembly());
            var soundPlayer = new SoundPlayer();
            Console.WriteLine("Internet Check Started");

            while (IsRunning)
            {
                Thread.Sleep(DelayScrubber.SetDelay(options));
                AppNetworkListUser nlmUser = new AppNetworkListUser();

                if (!online && nlmUser.NLM.IsConnectedToInternet)
                {
                    online = true;
                    soundPlayer.Stream = CheckInternetOnlineResources.NetUp;
                    soundPlayer.Play();
                    Console.WriteLine("Internet is on. Do your thing!");
                }
                else if (online && !nlmUser.NLM.IsConnectedToInternet)
                {
                    online = false;
                    soundPlayer.Stream = CheckInternetOnlineResources.NetDown;
                    soundPlayer.Play();
                    Console.WriteLine("Prepare for war! Net's Down");
                }
            }
        }
    }
}